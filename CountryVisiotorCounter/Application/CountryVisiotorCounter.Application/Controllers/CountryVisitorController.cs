﻿namespace CountryVisiotorCounter.Application.Controllers
{
    using CountryVisiotorCounter.Core.Abstract;
    using CountryVisiotorCounter.Core.Contracts;
    using CountryVisiotorCounter.Core.Dto;
    using CountryVisiotorCounter.Core.Exceptions;
    using Microsoft.AspNetCore.Mvc;
    
    [Route("[controller]")]
    public class CountryVisitorController : Controller
    {
        private readonly ICountryVisitorService _countryVisitorService;
        private readonly ILogger<CountryVisitorController> _logger;

        public CountryVisitorController(ILogger<CountryVisitorController> logger, ICountryVisitorService countryVisitorService)
        {
            _logger = logger;
            _countryVisitorService = countryVisitorService;
        }

        [HttpPost("File")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {

            if (file.Length == 0)
            {
                return BadRequest("The file is required");
            }

            try
            {
                using(var contentStream = new StreamReader(file.OpenReadStream()))
                {
                    var content = contentStream.ReadToEnd();
                    _countryVisitorService.AddFile(new FileContract(file.FileName, content));
                }

               return Ok();
            }
            catch (CommonException ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest("Internal error");
            }
        }

        [HttpGet("File/Processed")]
        public IActionResult GetProcesedFilesQuantity()
        {
            try
            {
                int result = _countryVisitorService.GetProcesedFiles();
                return Ok(result);
            }
            catch (CommonException ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest("Internal error");
            }
        }

        [HttpGet()]
        public IActionResult GetCountryVisitors()
        {
            try
            {
                List<CounrtyViewersDto> result =  _countryVisitorService.GetCountryVisitors();
                return Ok(result);

            }
            catch (CommonException ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return BadRequest("Internal error");
            }
        }
    }
}
