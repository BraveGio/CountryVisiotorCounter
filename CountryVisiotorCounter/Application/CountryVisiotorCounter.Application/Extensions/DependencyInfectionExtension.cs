﻿namespace CountryVisiotorCounter.Application.Extensions
{
    using CountryVisiotorCounter.Core.Abstract;
    using CountryVisiotorCounter.Core.Services;
    using CountryVisiotorCounter.Infrastructure.LocalStorage;
    using CountryVisiotorCounter.Infrastructure.Mssql.Repository;

    public static class DependencyInfectionExtension
    {
        public static IServiceCollection RegisterApplicationDI(this IServiceCollection service)
        {
            service.AddTransient<IFileStorage, LocalFileStorage>();
            service.AddTransient<IProcessFilesService, ProcessFIlesService>();
            service.AddTransient<ICountryViewersRepository, CountryViewersRepository>();
            service.AddTransient<ICountryVisitorService, CountryVisitorService>();

            return service;
        }
    }
}
