﻿namespace CountryVisiotorCounter.Worker.Extensions
{
    using Hangfire;
    using Hangfire.SqlServer;

    public static class HangfireExtension
    {
        public static IServiceCollection AddHangfire(this IServiceCollection services)
        {
            services.AddHangfire(configuration => configuration
            .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
            .UseSimpleAssemblyNameTypeSerializer()
            .UseRecommendedSerializerSettings()
            .UseSqlServerStorage(Environment.GetEnvironmentVariable("MSSQL_DB_CONNECTION_STRING"), new SqlServerStorageOptions
            {
                CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                QueuePollInterval = TimeSpan.Zero,
                UseRecommendedIsolationLevel = true,
                DisableGlobalLocks = true
            }));

            services.AddHangfireServer();

            return services;
        }
    }
}
