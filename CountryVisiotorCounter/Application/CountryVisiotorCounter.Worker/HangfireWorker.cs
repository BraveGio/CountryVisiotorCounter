namespace CountryVisiotorCounter.Worker
{
    using CountryVisiotorCounter.Core.Abstract;
    using Hangfire;
    
    public class HangfireWorker : BackgroundService
    {
        public HangfireWorker()
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                RecurringJob.AddOrUpdate<IProcessFilesService>("Process files", x => x.Process(), "* * * * * *");
            }
        }
    }
}