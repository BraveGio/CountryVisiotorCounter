using CountryVisiotorCounter.Infrastructure.Mssql.Extensions;
using CountryVisiotorCounter.Worker;
using CountryVisiotorCounter.Worker.Extensions;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.RegisterMSSQLContext();
        services.RegisterApplicationDI();
        services.AddHangfire();
        services.AddHostedService<HangfireWorker>();
    })
    .Build();

await host.RunAsync();
