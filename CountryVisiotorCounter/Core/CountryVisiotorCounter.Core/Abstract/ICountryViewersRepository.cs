﻿using CountryVisiotorCounter.Core.Dto;
using CountryVisiotorCounter.Core.Models;

namespace CountryVisiotorCounter.Core.Abstract
{
    public interface ICountryViewersRepository
    {
        public void Add(CountryViewers counrtyViewers);
        public List<CounrtyViewersDto> GetByCountry();
        public int GetRecordQuantity();
    }
}
