﻿namespace CountryVisiotorCounter.Core.Abstract
{
    using CountryVisiotorCounter.Core.Contracts;
    using CountryVisiotorCounter.Core.Dto;

    public interface ICountryVisitorService
    {
        public void AddFile(FileContract fileContract);
        public List<CounrtyViewersDto> GetCountryVisitors();
        public int GetProcesedFiles();
    }
}
