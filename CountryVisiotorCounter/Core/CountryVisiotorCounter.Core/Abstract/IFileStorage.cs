﻿namespace CountryVisiotorCounter.Core.Abstract
{
    using CountryVisiotorCounter.Core.Contracts;
    
    public interface IFileStorage
    {
        public void Save(FileContract file);
        public void Delete(string fileName);
        public FileContract? GetFile(string fileName);
        public List<string> GetFilesNames();
    }
}
