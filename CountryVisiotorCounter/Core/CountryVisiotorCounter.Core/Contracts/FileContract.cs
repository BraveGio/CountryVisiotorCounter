﻿namespace CountryVisiotorCounter.Core.Contracts
{
    public class FileContract
    {
        public string Name { get; set; }
        public string Content { get; set; }

        public FileContract(string name, string content)
        {
            Name = name;
            Content = content;
        }

    }
}
