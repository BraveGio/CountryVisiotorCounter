﻿using CountryVisiotorCounter.Core.Exceptions;

namespace CountryVisiotorCounter.Core.Models
{
    public class CountryViewers
    {
        public int Visitors { get; set; }
        public string? Country { get; set; }
        public DateTime Date { get; set; }

        public bool Validate(out List<string> errors)
        {
            errors = new List<string>();

            if (string.IsNullOrEmpty(Country))
                errors.Add("Country can not be null or empty");

            if(Visitors == null)
                errors.Add("Visitors can not be null");

            if (Visitors != null && Visitors < 0 )
                errors.Add("Visitors can not be negative");


            if (Date == null)
                errors.Add("Date can not be null");

            if (errors.Any())
                throw new CommonException(string.Join(",", errors));

            return true;
        }
    }
}
