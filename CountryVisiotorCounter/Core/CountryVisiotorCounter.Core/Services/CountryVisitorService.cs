﻿namespace CountryVisiotorCounter.Core.Services
{
    using CountryVisiotorCounter.Core.Abstract;
    using CountryVisiotorCounter.Core.Contracts;
    using CountryVisiotorCounter.Core.Dto;
    using CountryVisiotorCounter.Core.Exceptions;
    using CountryVisiotorCounter.Core.Models;
    using Newtonsoft.Json;

    public class CountryVisitorService : ICountryVisitorService
    {
        private readonly ICountryViewersRepository _repository;
        private readonly IFileStorage _fileStorage;

        public CountryVisitorService(ICountryViewersRepository repository, IFileStorage fileStorage)
        {
            _repository = repository;
            _fileStorage = fileStorage;
        }

        public void AddFile(FileContract file)
        {
            if(file == null)
                throw new CommonException("Internal error");

            if(Path.GetExtension(file.Name).ToLower() != ".json")
                throw new CommonException("Wrong file extension");

            if(!IsModelValid(file))
                throw new CommonException("Invalid file format");

            _fileStorage.Save(file);
        }

        public List<CounrtyViewersDto> GetCountryVisitors()
        {
            return _repository.GetByCountry();
        }

        public int GetProcesedFiles()
        {
            return _repository.GetRecordQuantity();
        }

        private bool IsModelValid(FileContract file)
        {
            var result = JsonConvert.DeserializeObject<CountryViewers>(file.Content);

            return result != null && result.Validate(out List<string> errors );
        }
    }
}
