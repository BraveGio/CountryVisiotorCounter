﻿namespace CountryVisiotorCounter.Core.Services
{
    using CountryVisiotorCounter.Core.Abstract;
    using CountryVisiotorCounter.Core.Contracts;
    using CountryVisiotorCounter.Core.Models;
    using Newtonsoft.Json;

    public class ProcessFIlesService : IProcessFilesService
    {
        public IFileStorage _fileStorage;
        private readonly ICountryViewersRepository _repository;

        public ProcessFIlesService(IFileStorage fileStorage, ICountryViewersRepository repository)
        {
            _fileStorage = fileStorage;
            _repository = repository;
        }

        public void Process()
        {
            List<string> storedFilesName = _fileStorage.GetFilesNames();

            foreach (string storedFileName in storedFilesName)
            {
                FileContract? fileContract = _fileStorage.GetFile(storedFileName);

                if(fileContract == null)
                    continue;

                Add(fileContract);

                _fileStorage.Delete(storedFileName);
            }
        }

        private void Add(FileContract fileContract)
        {
            CountryViewers? result = JsonConvert.DeserializeObject<CountryViewers>(fileContract.Content);

            if(result == null)
                return;

            _repository.Add(result);
        }
    }
}
