﻿namespace CountryVisiotorCounter.Infrastructure.LocalStorage
{
    using CountryVisiotorCounter.Core.Abstract;
    using CountryVisiotorCounter.Core.Contracts;
    using CountryVisiotorCounter.Core.Exceptions;

    public class LocalFileStorage : IFileStorage
    {
        public LocalFileStorage()
        {
        }

        public void Delete(string fileName)
        {
            var basePath = Environment.GetEnvironmentVariable("LOCAL_FILE_STORAGE_PATH");

            if (string.IsNullOrEmpty(basePath))
                throw new CommonException("Wrong local file storage path");

            var filePath = Path.Combine(basePath, fileName);

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        public void Save(FileContract file)
        {
            var basePath = Environment.GetEnvironmentVariable("LOCAL_FILE_STORAGE_PATH");
            if (string.IsNullOrEmpty(basePath))
                throw new CommonException("Wrong local file storage path");

            var filePath = Path.Combine(basePath, Guid.NewGuid + file.Name);
            using (var destinationFile = new StreamWriter(filePath))
            {
                destinationFile.Write(file.Content);
                destinationFile.Close();
            }
        }

        public FileContract? GetFile(string fileName)
        {
            var basePath = Environment.GetEnvironmentVariable("LOCAL_FILE_STORAGE_PATH");
            if (string.IsNullOrEmpty(basePath))
                throw new CommonException("Wrong local file storage path");

            var filePath = Path.Combine(basePath, fileName);

            if (File.Exists(filePath))
            {
                using (var streamReader = new StreamReader(filePath))
                {
                    return new FileContract(fileName, streamReader.ReadToEnd());
                }
            }

            return null;
        }

        public List<string> GetFilesNames()
        {
            var basePath = Environment.GetEnvironmentVariable("LOCAL_FILE_STORAGE_PATH");

            var filesPath = Directory.GetFiles(basePath);

            return filesPath.Select(x => x.Replace(basePath + "/", "")).ToList();
        }

    }
}
