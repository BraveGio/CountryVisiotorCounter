﻿namespace CountryVisiotorCounter.Infrastructure.Mssql.Builders
{
    using CountryVisiotorCounter.Infrastructure.Mssql.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal class CountryVisitorMssqlEntityBuilder
    {
        public static void CreateBuilder(ModelBuilder builder)
        {
            new CountryVisitorMssqlEntityBuilder().Build(builder.Entity<CountryVisitorMssqlEntity>());
        }

        protected virtual void Build(EntityTypeBuilder<CountryVisitorMssqlEntity> builder)
        {

            builder.ToTable("Db", "CountryVisitor");

            builder.Property(x => x.Id)
                .ValueGeneratedOnAdd()
                .IsRequired();

            builder.HasKey(x => x.Id);

            builder.Property(x => x.CreatedAt)
                .HasDefaultValueSql("getdate()");

            builder.Property(res => res.Visitors)
                .IsRequired();

            builder.Property(res => res.Country)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(res => res.Date)
                 .IsRequired();

        }
    }
}
