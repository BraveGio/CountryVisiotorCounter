﻿namespace CountryVisiotorCounter.Infrastructure.Mssql.Context
{
    using CountryVisiotorCounter.Infrastructure.Mssql.Builders;
    using CountryVisiotorCounter.Infrastructure.Mssql.Entities;
    using Microsoft.EntityFrameworkCore;
    
    public class MssqlDbContext : DbContext
    {

        public MssqlDbContext(DbContextOptions<MssqlDbContext> options) : base(options)
        {

        }

        public DbSet<CountryVisitorMssqlEntity> CountryVisitor { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            CountryVisitorMssqlEntityBuilder.CreateBuilder(modelBuilder);
        }
    }
}
