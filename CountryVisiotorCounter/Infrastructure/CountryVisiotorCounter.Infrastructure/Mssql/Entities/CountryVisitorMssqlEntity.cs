﻿namespace CountryVisiotorCounter.Infrastructure.Mssql.Entities
{
    public class CountryVisitorMssqlEntity
    {
        public int Id { get; set; }
        public int Visitors { get; set; }
        public string? Country { get; set; }
        public DateTime Date { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
