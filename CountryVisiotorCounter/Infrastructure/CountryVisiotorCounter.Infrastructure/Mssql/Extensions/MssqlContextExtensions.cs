﻿using CountryVisiotorCounter.Infrastructure.Mssql.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace CountryVisiotorCounter.Infrastructure.Mssql.Extensions
{
    public static class MssqlContextExtensions
    {
        public static IServiceCollection RegisterMSSQLContext(this IServiceCollection services)
        {
            string? connectionString = Environment.GetEnvironmentVariable("MSSQL_DB_CONNECTION_STRING");

            if(connectionString == null)
                throw new ArgumentNullException(nameof(connectionString));

            services.AddDbContext<MssqlDbContext>(options =>
                options.UseSqlServer(connectionString));
                       
            return services;
        }
    }
}
