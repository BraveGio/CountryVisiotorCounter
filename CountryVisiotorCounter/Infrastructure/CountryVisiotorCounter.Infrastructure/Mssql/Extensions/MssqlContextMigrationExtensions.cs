﻿namespace CountryVisiotorCounter.Infrastructure.Mssql.Extensions
{
    using CountryVisiotorCounter.Infrastructure.Mssql.Context;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Serilog;

    public static class MssqlContextMigrationExtensions
    {
        public static IServiceCollection RegisterMSSQLMigrationContext(this IServiceCollection services)
        {
            Log.Information("Migrate database");
            IServiceProvider provider = services.BuildServiceProvider();

            MssqlDbContext? context = provider.GetService<MssqlDbContext>();

            if (context != null)
            {
                context.Database.Migrate();
                context.Database.EnsureCreated();
                Log.Information("Migrate database ended");
            }
            else
            {
                Log.Information("Unable to execute database migrate ");
            }

            return services;
        }
    }
}
