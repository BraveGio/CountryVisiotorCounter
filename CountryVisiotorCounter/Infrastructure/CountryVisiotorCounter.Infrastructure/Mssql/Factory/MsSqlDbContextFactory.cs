﻿namespace CountryVisiotorCounter.Infrastructure.Mssql.Factory
{
    using CountryVisiotorCounter.Infrastructure.Mssql.Context;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;

    internal class MsSqlDbContextFactory : IDesignTimeDbContextFactory<MssqlDbContext>
    {
        private static string _connectionString;

        public MsSqlDbContextFactory()
        {
            string? connectionString = Environment.GetEnvironmentVariable("MSSQL_DB_CONNECTION_STRING");

            if (string.IsNullOrEmpty(connectionString))
            {
                _connectionString =
                    "Server=localhost\\SQLEXPRESS,1433;Database=CVC;User=sa;password=P@ssword123!;Trusted_Connection=False;MultipleActiveResultSets=true";
            }
            else
            {
                _connectionString = connectionString;
            }
        }

        public MssqlDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<MssqlDbContext>();
            optionsBuilder.UseSqlServer(_connectionString);

            return new MssqlDbContext(optionsBuilder.Options);
        }
    }
}
