﻿namespace CountryVisiotorCounter.Infrastructure.Mssql.Mappers
{
    using CountryVisiotorCounter.Core.Models;
    using CountryVisiotorCounter.Infrastructure.Mssql.Entities;

    public static class CountryViewersMapper
    {

        public static CountryVisitorMssqlEntity Map(this CountryViewers countryViewers)
        {
            return new CountryVisitorMssqlEntity
            {
                Visitors = countryViewers.Visitors,
                Date = countryViewers.Date,
                Country = countryViewers.Country,
                CreatedAt = DateTime.UtcNow,
            };
        }
    }
}
