﻿namespace CountryVisiotorCounter.Infrastructure.Mssql.Repository
{
    using CountryVisiotorCounter.Core.Abstract;
    using CountryVisiotorCounter.Core.Dto;
    using CountryVisiotorCounter.Core.Models;
    using CountryVisiotorCounter.Infrastructure.Mssql.Context;
    using CountryVisiotorCounter.Infrastructure.Mssql.Mappers;

    public class CountryViewersRepository : ICountryViewersRepository
    {

        protected readonly MssqlDbContext _context;

        public CountryViewersRepository(MssqlDbContext context)
        {
            _context = context;
        }

        public void Add(CountryViewers countryViewers)
        {
            _context.CountryVisitor.Add(countryViewers.Map());

            _context.SaveChanges();

        }

        public List<CounrtyViewersDto> GetByCountry()
        {

            return _context.CountryVisitor
                .GroupBy(res => res.Country, 
                                (key, records) => new CounrtyViewersDto 
                    { 
                        Country = key,
                        Visitors = records.Sum(res => res.Visitors)
                    }
               ).ToList();
        }

        public int GetRecordQuantity()
        {
            return _context.CountryVisitor.Count();
        }
    }
}
